GIT_BRANCH := $(shell git branch | grep "^\*" | sed 's/^..//')
GIT_DESCRIBE := $(shell git describe --dirty --always)
BUILD_DATE := $(shell date +"%Y-%b-%d")
BUILD_TIME := $(shell date +"%T")

all:
	@echo $(GIT_BRANCH)
	@echo $(GIT_DESCRIBE)
	@echo $(BUILD_DATE)
	@echo $(BUILD_TIME)
